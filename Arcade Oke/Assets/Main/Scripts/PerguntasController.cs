﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;

public class PerguntasController : MonoBehaviour {

    private PerguntasData perguntasData;
    public int perguntaCount;
    public int perguntaLastID;


    void Start () {
        StartCoroutine (LoadPerguntasData ());
    }

    IEnumerator LoadPerguntasData () {
        string filePath = DataManager.getInstance().folderConteudo + DataManager.getInstance().urlPerguntas;
        string url = Path.Combine(Application.streamingAssetsPath, filePath);
        //string url = DataManager.getInstance ().serverUrl + DataManager.getInstance ().urlPerguntas;
        string dataAsJson;
        if (url.Contains ("://") || url.Contains (":///")) {
            UnityWebRequest www = UnityWebRequest.Get (url);
            yield return www.SendWebRequest ();
            dataAsJson = www.downloadHandler.text;
        } else {
            dataAsJson = File.ReadAllText (url);
        }

        perguntasData = JsonUtility.FromJson<PerguntasData> (dataAsJson);

    }

    public string GetPergunta() {
        Debug.Log("GetPergunta");
        int i;
        if(perguntaCount == 0) {
            i = Random.Range(0, perguntasData.allPerguntasData.Length);
        } else {
            i = perguntaLastID + 1;
            if(i>=perguntasData.allPerguntasData.Length) {
                i = 0;
            }
        }
        perguntaLastID = i;
        perguntaCount += 1;
        return perguntasData.allPerguntasData[i].Pergunta.ToString();
    }
}