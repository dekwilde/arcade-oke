﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.IO;


public class MainController : MonoBehaviour
{

    public GameObject homePanel;
    public GameObject videoPanel;

    public VideoPlayer videoPlayer;

    public GameObject recordPanel;
    public Text perguntaText;
    public Text stateText;
    private PerguntasController perguntasController;
    private RecorderController recorderController;
    private NavigationController navigationController;

    void Start() {
        DataManager.getInstance().state = DataManager.State.Idle;
        perguntasController = this.gameObject.GetComponent<PerguntasController>();
        recorderController = this.gameObject.GetComponent<RecorderController>();
        navigationController = this.gameObject.GetComponent<NavigationController>();
        ShowHomePanel();
    }

    public void StartNavigation() {
        navigationController.StartSerialPort();
    }

    void ResetPanels() {
        homePanel.SetActive(false);
        videoPanel.SetActive(false);
        recordPanel.SetActive(false);
    }

    public void PlayVideo(string url) {
        DataManager.getInstance().state = DataManager.State.Playing;
        ResetPanels();      
        videoPanel.SetActive(true);
        string filePath = DataManager.getInstance().folderConteudo + DataManager.getInstance().urlVideos + url;
        videoPlayer.url = Path.Combine(Application.streamingAssetsPath, filePath);
        videoPlayer.Play();
    }

    public void StopVideo() {
        DataManager.getInstance().state = DataManager.State.Idle;
        videoPlayer.Stop();
        ShowHomePanel();
    }

    public void ShowHomePanel() {
        DataManager.getInstance().state = DataManager.State.Idle;
        ResetPanels();
        homePanel.SetActive(true);
        perguntasController.perguntaCount = 0;
    }

    public void ShowRecordPanel() {
        DataManager.getInstance().state = DataManager.State.Record;
        ResetPanels();
        recordPanel.SetActive(true);
        perguntaText.text = perguntasController.GetPergunta();
        stateText.text = "aperte VERDE para começar GRAVAR";
        
    }

    public void StartRecord() {
        DataManager.getInstance().state = DataManager.State.Recording;
        stateText.text = "GRAVANDO\nvermelho para PARAR";
        recorderController.StartRecording(perguntasController.perguntaLastID+1);
    }

    public void StopRecord() {
        DataManager.getInstance().state = DataManager.State.Asking;
        stateText.text = "SALVANDO";
        recorderController.StopRecording();
        StartCoroutine (NextPergunta ());

    }


    private IEnumerator NextPergunta() {
        yield return new WaitForSeconds(2.0f);
        if(perguntasController.perguntaCount == 3) {
            perguntaText.text = "OBRIGADO(A) POR PARTICIPAR";
            stateText.text = "FIM";
            yield return new WaitForSeconds(6.0f);
            ShowHomePanel();
        } else {
            perguntaText.text = "PROCURANDO NOVA PERGUNTA";
            stateText.text = "AGUARDE";
            yield return new WaitForSeconds(3.0f);
            ShowRecordPanel();
        }
    }

}
