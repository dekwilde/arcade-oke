﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO.Ports;

public class NavigationController : MonoBehaviour
{
    // Start is called before the first frame update


    private int selectedMusicaButton = 0;
    public GameObject musicasList;
    private MainController mainController;
    SerialPort sp = new SerialPort();

    void Start()
    {
        mainController = this.gameObject.GetComponent<MainController>();
    }

    public void StartSerialPort() {
        sp = new SerialPort(DataManager.getInstance().configData.SerialPortCOM, 9600);
        sp.Open();
        sp.ReadTimeout = 1;
    }


    void Update()
    {
        if (sp.IsOpen)
        {
            try
            {
                SPCodeInput(sp.ReadByte());
                print(sp.ReadByte());
            }
            catch (System.Exception)
            {

            }
        }

        KeybordInput();
    }


    void SPCodeInput(int SPCode)
    {
        if (SPCode == 1)
        {
            MusicaNext();
        }
        if (SPCode == 2)
        {
            MusicaPrevious();
        }
        if (SPCode == 3)
        {
            KeyCancel();
        }
        if (SPCode == 4)
        {
            KeyOK();
        }
    }


    void KeybordInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            MusicaPrevious();
        }
        else if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            MusicaNext();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            KeyOK();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            KeyCancel();
        }
    }

    void KeyOK()
    {
        if (DataManager.getInstance().state == DataManager.State.Idle)
        {
            MusicaSelect();
        }
        if (DataManager.getInstance().state == DataManager.State.Record)
        {
            mainController.StartRecord();
        }
    }

    void KeyCancel()
    {
        if (DataManager.getInstance().state == DataManager.State.Playing)
        {
            mainController.StopVideo();
        }
        if (DataManager.getInstance().state == DataManager.State.Recording)
        {
            mainController.StopRecord();
        }
    }

    void MusicaSelect()
    {
        GameObject musicaButton = musicasList.transform.GetChild(selectedMusicaButton).gameObject;
        musicaButton.GetComponent<MusicaButtonController>().HandleClick();
    }

    void MusicaNext()
    {
        selectedMusicaButton++;
        if (selectedMusicaButton >= musicasList.transform.childCount)
        {
            selectedMusicaButton = musicasList.transform.childCount - 1;
        }
        GameObject musicaButton = musicasList.transform.GetChild(selectedMusicaButton).gameObject;
        musicaButton.GetComponent<Button>().Select();
        //musicaButton.GetComponent<Image>().color = Color.blue;
    }

    void MusicaPrevious()
    {
        selectedMusicaButton--;
        if (selectedMusicaButton < 0)
        {
            selectedMusicaButton = 0;
        }
        GameObject musicaButton = musicasList.transform.GetChild(selectedMusicaButton).gameObject;
        musicaButton.GetComponent<Button>().Select();
        //musicaButton.GetComponent<Image>().color = Color.blue;

    }
}
