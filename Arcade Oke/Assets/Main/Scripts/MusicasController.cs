﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class MusicasController : MonoBehaviour {

     public Transform musicaParent;
     public SimpleObjectPool musicaPool;

     private List<GameObject> musicasList = new List<GameObject> ();

     private MusicasData MusicasDataPool;

     void Start () {
          StartCoroutine (LoadMusicasData ());
     }

     IEnumerator LoadMusicasData () {
          string filePath = DataManager.getInstance ().folderConteudo + DataManager.getInstance ().urlMusicas;
          string url = Path.Combine (Application.streamingAssetsPath, filePath);
          //string url = DataManager.getInstance().serverUrl + DataManager.getInstance().urlMusicas;

          //Check if we should use UnityWebRequest or File.ReadAllBytes
          string dataAsJson;
          if (url.Contains ("://") || url.Contains (":///")) {
               UnityWebRequest www = UnityWebRequest.Get (url);
               yield return www.SendWebRequest ();
               dataAsJson = www.downloadHandler.text;
          } else {
               dataAsJson = File.ReadAllText (url);
          }

          MusicasDataPool = JsonUtility.FromJson<MusicasData> (dataAsJson);

          for (int i = 0; i < MusicasDataPool.allMusicasData.Length; i++) {

               GameObject musicaPrefab = musicaPool.GetObject ();
               musicasList.Add (musicaPrefab);
               musicaPrefab.transform.SetParent (musicaParent);
               musicaPrefab.transform.localScale = Vector3.one;

               string getNome = MusicasDataPool.allMusicasData[i].Nome;
               string getUrl = MusicasDataPool.allMusicasData[i].Url;
               Debug.Log ("url video " + getUrl);

               MusicaButtonController musicaButtonController = musicaPrefab.GetComponent<MusicaButtonController> ();
               musicaButtonController.Setup (getNome, getUrl);

          }

     }

}