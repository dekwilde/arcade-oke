﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RecorderController : MonoBehaviour {

    public int RecordingDeviceIndex = 0;
    string InputDevice { get { return Microphone.devices[0]; } }
    AudioClip RecordingClip;
    public float TrimCutoff = .01f;
    int SampleRate = 44100;
    float Timer = 0;

    private MainController mainController;

    private string filename;


    // Start is called before the first frame update
    void Start () {
        mainController = this.gameObject.GetComponent<MainController>();
    }

    // Update is called once per frame
    void Update () {
        if (DataManager.getInstance().state == DataManager.State.Recording) {
            Timer += Time.deltaTime;
            if (Timer >= DataManager.getInstance ().configData.MaxClipDuration) {
                mainController.StopRecord();
            }      
        }
    }

    public void StartRecording (int perguntaID) {
        Debug.Log("StartRecording");
        long time = new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds();
        filename = "Pergunta" + perguntaID.ToString() + "-" + time.ToString();
        Timer = 0;
        RecordingClip = Microphone.Start (InputDevice, true, DataManager.getInstance ().configData.MaxClipDuration, SampleRate);
    }

    public void StopRecording () {
        Debug.Log("StopRecording");
        Timer = 0;
        Microphone.End (InputDevice);
        AudioClip trimmedClip = SavWav.TrimSilence (RecordingClip, TrimCutoff);
        string audioFilePath = filename + ".wav";
        SavWav.Save (audioFilePath, trimmedClip);
    }

    [ContextMenu ("Recording Devices")]
    void PrintAllRecordingDevices () {
        for (int i = 0; i < Microphone.devices.Length; i++)
            print ("Recorindg devices: " + Microphone.devices[i] + " " + i);
    }
}