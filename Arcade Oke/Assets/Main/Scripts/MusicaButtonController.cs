﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicaButtonController : MonoBehaviour
{
    public Text musicaText;

    private MainController mainController;

    private string loadNome;
    private string loadUrl;

    // Use this for initialization
    void Start()
    {
        mainController = FindObjectOfType<MainController>();
    }

    public void Setup(string nome, string url)
    {
        loadNome = nome;
        loadUrl = url;
        musicaText.text = loadNome;
    }


    public void HandleClick()
    {
        mainController.PlayVideo(loadUrl);
    }
}
