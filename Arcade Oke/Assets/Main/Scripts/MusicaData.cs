﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MusicaData
{
    public string Nome;
    public string Url;
}