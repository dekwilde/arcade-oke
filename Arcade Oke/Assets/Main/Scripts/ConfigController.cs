﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class ConfigController : MonoBehaviour {

    private ConfigData configData;
    private MainController mainController;

    void Start () {
        mainController = this.gameObject.GetComponent<MainController> ();
        StartCoroutine (LoadConfigData ());
    }

    IEnumerator LoadConfigData () {
        string filePath = DataManager.getInstance ().folderConteudo + DataManager.getInstance ().urlConfig;
        string url = Path.Combine (Application.streamingAssetsPath, filePath);
        //Check if we should use UnityWebRequest or File.ReadAllBytes
        string dataAsJson;
        if (url.Contains ("://") || url.Contains (":///")) {
            UnityWebRequest www = UnityWebRequest.Get (url);
            yield return www.SendWebRequest ();
            dataAsJson = www.downloadHandler.text;
        } else {
            dataAsJson = File.ReadAllText (url);
        }

        DataManager.getInstance ().configData = JsonUtility.FromJson<ConfigData> (dataAsJson);
        mainController.StartNavigation ();
    }
}