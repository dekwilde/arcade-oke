﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ConfigData
{
    public string SerialPortCOM;
    public int MaxClipDuration;
}