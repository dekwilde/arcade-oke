﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager {
     private static DataManager instance;
     public static DataManager getInstance () {
          if (instance == null) {
               instance = new DataManager ();
          }
          return instance;
     }

          
     public string urlMusicas = "/musicas.json";

     public string urlVideos = "videos/";

     public string folderGravacoes = "gravacoes/";

     public string folderConteudo = "conteudo/";

     public string urlPerguntas = "/perguntas.json";

     public string urlConfig = "/config.json";

     public ConfigData configData;

    public enum State {
          Idle,
          Playing,
          Record,
          Recording,
          Asking
    }

    public State state;

}